package com.example.googlesearch

import com.example.googlesearch.data.database.dao.SearchResultDao
import com.example.googlesearch.data.database.model.SearchResultRoom
import com.example.googlesearch.data.network.GoogleApi
import com.example.googlesearch.data.network.model.GoogleSearchResponse
import com.example.googlesearch.data.network.model.GoogleSearchResult
import com.example.googlesearch.repository.SearchRepository
import com.example.googlesearch.repository.mappers.toRoom
import com.example.googlesearch.repository.mappers.toSearchResult
import com.example.googlesearch.repository.model.Result
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collectIndexed
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.Response

@ExperimentalCoroutinesApi
class SearchRepositoryTest {

    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    @MockK
    private lateinit var googleApi: GoogleApi

    @MockK
    private lateinit var searchResultDao: SearchResultDao

    @MockK
    private lateinit var networkResponse: Response<GoogleSearchResponse>

    private lateinit var repository: SearchRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this)
        repository = SearchRepository(googleApi, searchResultDao)
    }

    @Test
    fun `search test`() = coroutineTestRule.runBlockingTest {
        val networkData = getNetworkResult()
        val localData: List<SearchResultRoom> = networkData.toRoom("query")
        val result = localData.map { it.toSearchResult() }
        every { networkResponse.body() } returns networkData
        every { networkResponse.isSuccessful } returns true
        coEvery { searchResultDao.getResults(any(), any()) } answers
                { flowOf(emptyList()) } andThen
                { flowOf(localData) }
        coJustRun { searchResultDao.insert(any()) }

        coEvery { googleApi.search(any(), any()) } returns networkResponse

        repository.search("query").take(2).collectIndexed { index, value ->
            if (index == 0) {
                assert(value is Result.Loading)
                assert((value as Result.Loading).result?.isEmpty() == true)
            } else {
                assert(value is Result.Success)
                assert((value as Result.Success).result == result)
            }
        }

        coVerify(exactly = 1) { googleApi.search(any(), any()) }
        coVerify(exactly = 2) { searchResultDao.getResults(any(), any()) }
        coVerify(exactly = 1) { searchResultDao.insert(any()) }
        coVerify(exactly = 0) { searchResultDao.delete() }
    }

    @Test
    fun `search test error`() = coroutineTestRule.runBlockingTest {
        every { networkResponse.body() } returns null
        every { networkResponse.isSuccessful } returns false
        every { networkResponse.code() } returns 400
        coEvery { searchResultDao.getResults(any(), any()) } answers { flowOf(emptyList()) }

        coEvery { googleApi.search(any(), any()) } returns networkResponse
        coJustRun { searchResultDao.delete() }

        repository.search("query").take(2).collectIndexed { index, value ->
            if (index == 0) {
                assert(value is Result.Loading)
                assert((value as Result.Loading).result?.isEmpty() == true)
            } else {
                assert(value is Result.Error)
            }
        }

        coVerify(exactly = 1) { googleApi.search(any(), any()) }
        coVerify(exactly = 1) { searchResultDao.getResults(any(), any()) }
        coVerify(exactly = 0) { searchResultDao.insert(any()) }
        coVerify(exactly = 1) { searchResultDao.delete() }
    }

    @Test
    fun `search test not found`() = coroutineTestRule.runBlockingTest {
        every { networkResponse.body() } returns null
        every { networkResponse.isSuccessful } returns false
        every { networkResponse.code() } returns 404
        coEvery { searchResultDao.getResults(any(), any()) } answers { flowOf(emptyList()) }

        coEvery { googleApi.search(any(), any()) } returns networkResponse
        coJustRun { searchResultDao.delete() }

        repository.search("query").take(2).collectIndexed { index, value ->
            if (index == 0) {
                assert(value is Result.Loading)
                assert((value as Result.Loading).result?.isEmpty() == true)
            } else {
                assert(value is Result.NotFound)
            }
        }

        coVerify(exactly = 1) { googleApi.search(any(), any()) }
        coVerify(exactly = 1) { searchResultDao.getResults(any(), any()) }
        coVerify(exactly = 0) { searchResultDao.insert(any()) }
        coVerify(exactly = 1) { searchResultDao.delete() }
    }

    private fun getNetworkResult(): GoogleSearchResponse {
        return mockk<GoogleSearchResponse>().apply {
            every { results } returns listOf(
                mockk<GoogleSearchResult>().apply {
                    every { title } returns "Android"
                    every { link } returns "link"
                    every { description } returns "descriptiom"
                }
            )
        }
    }
}