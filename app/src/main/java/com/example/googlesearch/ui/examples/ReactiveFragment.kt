package com.example.googlesearch.ui.examples

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.googlesearch.R
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import java.text.SimpleDateFormat
import java.util.*


class ReactiveFragment : Fragment() {

    private val viewModel: ReactiveViewModel by lazy {
        ViewModelProvider(
            this,
            ReactiveViewModelFactory()
        ).get(ReactiveViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.stateFlowOutput.collect {
                view?.findViewById<TextView>(R.id.stateFlowResult)?.text = it.toString()
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.sharedFlowOutput.collect {
                view?.findViewById<TextView>(R.id.sharedFlowResult)?.text = it.toString()
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.timerStateFlow.collect {
                view?.findViewById<TextView>(R.id.stateFlowTimerResult)?.text = it.toTime()
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.timerSharedFlow.collect {
                view?.findViewById<TextView>(R.id.sharedFlowTimerResult)?.text = it.toTime()
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reactive, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.addItemButton).setOnClickListener { viewModel.addItem() }
        view.findViewById<Button>(R.id.newConsumerButton).setOnClickListener { addConsumer() }
        view.findViewById<Button>(R.id.newTimerConsumerButton).setOnClickListener { addTimerConsumer() }
        viewModel.liveDataOutput.observe(viewLifecycleOwner, Observer {
            view.findViewById<TextView>(R.id.liveDataResult)?.text = it.toString()
        })
        viewModel.timerLiveData.observe(viewLifecycleOwner, Observer {
            view.findViewById<TextView>(R.id.liveDataTimerResult)?.text = it.toTime()
        })
    }

    private fun addConsumer() {
        lifecycleScope.launch {
            viewModel.sharedFlowOutput.collect {
                delay(200)
                view?.findViewById<TextView>(R.id.secondSharedFlowResult)?.text = it.toString()
            }
        }
    }

    private fun addTimerConsumer() {
        lifecycleScope.launch {
            viewModel.timerSharedFlow.collect {
                delay(500)
                view?.findViewById<TextView>(R.id.secondSharedFlowTimerResult)?.text = it.toTime()
            }
        }
    }

    companion object {
        fun newInstance() =
            ReactiveFragment()
    }
}

@Suppress("UNCHECKED_CAST")
class ReactiveViewModelFactory : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ReactiveViewModel() as T
    }

}

@SuppressLint("SimpleDateFormat")
fun Long.toTime(): String {
    val date = Date(this)
    val format = SimpleDateFormat("HH:mm:ss")
    return format.format(date)
}