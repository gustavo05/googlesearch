package com.example.googlesearch.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.googlesearch.model.SearchUIModel
import com.example.googlesearch.model.UIState
import com.example.googlesearch.usecase.SearchUseCase
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.*

@FlowPreview
@ExperimentalCoroutinesApi
class SearchViewModel(searchUseCase: SearchUseCase) : ViewModel() {

    private val searchQuery = ConflatedBroadcastChannel<String>()

    private val search: StateFlow<SearchUIModel?> = searchQuery.asFlow()
        .flatMapLatest { searchUseCase.execute(query = it) }
        .flowOn(Dispatchers.IO)
        .stateIn(viewModelScope, SharingStarted.Lazily, null)

    val searchResult = search
        .filter { it != null }
        .map { it!! }
        .map { it.searchResult }
        .asLiveData()

    val imageResult = search
        .filter { it != null }
        .map { it!! }
        .map { it.imageResult }
        .asLiveData()

    fun search(query: String) {
        searchQuery.offer(query)
    }
}