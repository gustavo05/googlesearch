package com.example.googlesearch.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.googlesearch.R
import com.example.googlesearch.model.SearchResult

class SearchAdapter :
    ListAdapter<SearchResult, SearchAdapter.SearchViewHolder>(SearchResultDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.search_result_view, parent, false)
        return SearchViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class SearchViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(searchResult: SearchResult) {
            with(itemView) {
                findViewById<TextView>(R.id.searchResultTitle).text = searchResult.title
                findViewById<TextView>(R.id.searchResultDescription).text = searchResult.description
            }
        }
    }

    class SearchResultDiffUtil : DiffUtil.ItemCallback<SearchResult>() {
        override fun areContentsTheSame(oldItem: SearchResult, newItem: SearchResult): Boolean =
            oldItem == newItem


        override fun areItemsTheSame(oldItem: SearchResult, newItem: SearchResult): Boolean =
            oldItem.title == newItem.title
    }
}