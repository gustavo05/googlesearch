package com.example.googlesearch.ui.examples

import androidx.lifecycle.*
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.*

class ReactiveViewModel : ViewModel() {

    private var value = 0

    private val _liveDataOutput = MutableLiveData<Int>()
    val liveDataOutput: LiveData<Int> get() = _liveDataOutput

    private val _stateFlowOutput = MutableStateFlow(0)
    val stateFlowOutput: StateFlow<Int> get() = _stateFlowOutput

    private val _sharedFlowOutput = MutableSharedFlow<Int>(
        replay = 6,
        extraBufferCapacity = 0,
        onBufferOverflow = BufferOverflow.DROP_LATEST
    )
    val sharedFlowOutput = _sharedFlowOutput.asSharedFlow()

    private val timer = DummyNetwork.fetchDataCallback()
        .flowOn(Dispatchers.Default)

    val timerLiveData = timer.asLiveData()
    val timerStateFlow =
        timer.stateIn(viewModelScope, SharingStarted.WhileSubscribed(), System.currentTimeMillis())
    val timerSharedFlow = timer.shareIn(viewModelScope, SharingStarted.WhileSubscribed(), 15)


    fun addItem() {
        value++
        _liveDataOutput.value = value
        _stateFlowOutput.value = value
        _sharedFlowOutput.tryEmit(value)
    }
}

object DummyNetwork {
    fun fetchData() = flow {
        while (true) {
            delay(1000)
            emit(System.currentTimeMillis())
        }
    }

    fun fetchDataCallback() = callbackFlow<Long> {
        val dummyService = DummyService()
        dummyService.addListener(object : ServiceListener {
            override fun onUpdate(time: Long) {
                sendBlocking(time)
            }
        }
        )
        awaitClose { dummyService.removeListener() }
    }

    fun fetchDataChannel() = channelFlow<Long> {
        var job: Job? = null
        while (true) {
            job = launch {
                val time = getTime()
                sendBlocking(time)
            }
        }
        awaitClose { job?.cancel() }
    }

    private suspend fun getTime(): Long {
        delay(1000)
        return System.currentTimeMillis()
    }
}


class DummyService {
    private var listener: ServiceListener? = null
    suspend fun addListener(serviceListener: ServiceListener) {
        listener = serviceListener
        update()
    }

    fun removeListener() {
        listener = null
    }

    private suspend fun update() {
        while (listener != null) {
            delay(1000)
            listener?.onUpdate(System.currentTimeMillis())
        }
    }
}

interface ServiceListener {
    fun onUpdate(time: Long)
}
