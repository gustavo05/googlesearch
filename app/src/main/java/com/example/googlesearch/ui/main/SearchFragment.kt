package com.example.googlesearch.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.googlesearch.R
import com.example.googlesearch.di.provideSearchUseCase
import com.example.googlesearch.model.ImageResult
import com.example.googlesearch.model.SearchResult
import com.example.googlesearch.model.UIState
import com.example.googlesearch.ui.main.adapters.ImageAdapter
import com.example.googlesearch.ui.main.adapters.SearchAdapter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@ExperimentalCoroutinesApi
@FlowPreview
class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private lateinit var imageAdapter: ImageAdapter
    private lateinit var searchAdapter: SearchAdapter
    private lateinit var viewModel: SearchViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageAdapter = ImageAdapter()
        searchAdapter = SearchAdapter()
        view.findViewById<RecyclerView>(R.id.imagesList).apply {
            adapter = imageAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        }
        view.findViewById<RecyclerView>(R.id.searchList).apply {
            adapter = searchAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
        setupSearch()
    }

    private fun setupSearch() {
        val field = view?.findViewById<EditText>(R.id.searchField)
        field?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch(query = field.text.toString())
                field.clearFocus()
                val `in`: InputMethodManager? =
                    context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
                `in`?.hideSoftInputFromWindow(field.windowToken, 0)
                true
            } else {
                false
            }
        }
    }

    private fun performSearch(query: String) {
        viewModel.search(query)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(
            this,
            SearchViewModelFactory(requireContext())
        ).get(SearchViewModel::class.java)
        observe()
    }

    private fun observe() {
        viewModel.searchResult.observe(viewLifecycleOwner, Observer { loadSearchResults(it) })
        viewModel.imageResult.observe(viewLifecycleOwner, Observer { loadImageResults(it) })
    }

    private fun loadSearchResults(result: UIState<List<SearchResult>>) {
        when (result) {
            is UIState.Loading -> {
                if (result.result?.isNullOrEmpty() == true) {
                    setSearchError("Loading!!")
                } else {
                    submitSearchResults(result.result)
                }
            }
            is UIState.Success -> submitSearchResults(result.result)
            is UIState.NotFound -> setSearchError("Results not Found!")
            is UIState.Error -> setSearchError("Error getting results!")
        }
    }

    private fun submitSearchResults(data: List<SearchResult>?) {
        searchAdapter.submitList(data)
        view?.findViewById<TextView>(R.id.searchResultsError)?.visibility = View.GONE
        view?.findViewById<RecyclerView>(R.id.searchList)?.visibility = View.VISIBLE
    }

    private fun setSearchError(message: String) {
        view?.findViewById<TextView>(R.id.searchResultsError)?.apply {
            text = message
            visibility = View.VISIBLE
        }
        view?.findViewById<RecyclerView>(R.id.searchList)?.visibility = View.GONE
    }

    private fun loadImageResults(result: UIState<List<ImageResult>>) {
        when (result) {
            is UIState.Loading ->  {
                if (result.result?.isNullOrEmpty() == true) {
                    setImageError("Loading!!")
                } else {
                    submitImageResults(result.result)
                }
            }
            is UIState.Success -> submitImageResults(result.result)
            is UIState.NotFound -> setImageError("Images not Found!")
            is UIState.Error -> setImageError("Error getting images!")
        }
    }

    private fun submitImageResults(data: List<ImageResult>?) {
        imageAdapter.submitList(data)
        view?.findViewById<TextView>(R.id.imageResultsError)?.visibility = View.GONE
        view?.findViewById<RecyclerView>(R.id.imagesList)?.visibility = View.VISIBLE
    }

    private fun setImageError(message: String) {
        view?.findViewById<TextView>(R.id.imageResultsError)?.apply {
            text = message
            visibility = View.VISIBLE
        }
        view?.findViewById<RecyclerView>(R.id.imagesList)?.visibility = View.GONE
    }
}

@Suppress("UNCHECKED_CAST")
@ExperimentalCoroutinesApi
@FlowPreview
class SearchViewModelFactory(private val context: Context) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchViewModel(searchUseCase = provideSearchUseCase(context = context)) as T
    }

}