package com.example.googlesearch.ui.main.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.googlesearch.R
import com.example.googlesearch.model.ImageResult

class ImageAdapter : ListAdapter<ImageResult, ImageAdapter.ImageViewHolder>(ImageDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.image_result_view, parent, false)
        return ImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    inner class ImageViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(imageResult: ImageResult) {
            with(itemView) {
                Glide.with(this).load(imageResult.src).into(findViewById(R.id.imageResultView))
                findViewById<TextView>(R.id.imageTitle).text = imageResult.title
            }
        }
    }

    class ImageDiffUtil : DiffUtil.ItemCallback<ImageResult>() {

        override fun areContentsTheSame(oldItem: ImageResult, newItem: ImageResult): Boolean =
            oldItem.src == newItem.src

        override fun areItemsTheSame(oldItem: ImageResult, newItem: ImageResult): Boolean =
            oldItem.src == newItem.src

    }
}