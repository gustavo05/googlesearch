package com.example.googlesearch.repository.model

sealed class Result<out T> {
    data class Success<out T>(val result: T) : Result<T>()
    data class Loading<out T>(val result: T?) : Result<T>()
    object Error : Result<Nothing>()
    object NotFound : Result<Nothing>()
}