package com.example.googlesearch.repository

import com.example.googlesearch.repository.model.Result
import kotlinx.coroutines.flow.*
import retrofit2.Response

fun <DB, REMOTE, RESULT> fetchDataTemplate(
    fetchFromLocal: () -> Flow<DB>,
    shouldFetchFromRemote: (DB?) -> Boolean = { true },
    fetchFromRemote: suspend () -> Response<REMOTE>,
    saveRemoteData: suspend (REMOTE) -> Unit = { Unit },
    deleteData: suspend () -> Unit = { Unit },
    mapper: (DB) -> RESULT
) = flow {

    val localData = fetchFromLocal().first()

    if (shouldFetchFromRemote(localData)) {
        emit(Result.Loading(mapper(localData)))
        val remote = fetchFromRemote()
        if (remote.isSuccessful && remote.body() != null) {
            saveRemoteData(remote.body()!!)
            emitAll(fetchFromLocal().map { Result.Success(mapper(it)) })
        } else if (remote.code() == 404) {
            deleteData()
            emit(Result.NotFound)
        } else {
            deleteData()
            emit(Result.Error)
        }
    } else {
        emitAll(fetchFromLocal().map { Result.Success(mapper(it)) })
    }

}