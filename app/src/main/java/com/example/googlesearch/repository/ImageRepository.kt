package com.example.googlesearch.repository

import com.example.googlesearch.data.database.dao.ImageResultDao
import com.example.googlesearch.data.network.GoogleApi
import com.example.googlesearch.model.ImageResult
import com.example.googlesearch.model.SearchResult
import com.example.googlesearch.repository.mappers.toImageResult
import com.example.googlesearch.repository.mappers.toRoom
import com.example.googlesearch.repository.mappers.toSearchResult
import com.example.googlesearch.repository.model.Result
import kotlinx.coroutines.flow.*
import java.lang.Exception

class ImageRepository(
    private val googleApi: GoogleApi,
    private val imageResultDao: ImageResultDao
) {

    fun search(query: String): Flow<Result<List<ImageResult>>> =
        fetchDataTemplate(
            fetchFromLocal = { imageResultDao.getResults(query) },
            shouldFetchFromRemote = { true },
            fetchFromRemote = { googleApi.images(query = query) },
            saveRemoteData = { imageResultDao.insert(it.toRoom(query)) },
            deleteData = { imageResultDao.delete() },
            mapper = { it.map { item -> item.toImageResult() } }
        )
            .map { if (it is Result.Success && it.result.isEmpty()) Result.NotFound else it }
            .catch { emit(Result.Error) }
}