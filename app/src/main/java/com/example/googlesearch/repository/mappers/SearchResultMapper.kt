package com.example.googlesearch.repository.mappers

import com.example.googlesearch.data.database.model.SearchResultRoom
import com.example.googlesearch.data.network.model.GoogleSearchResponse
import com.example.googlesearch.model.SearchResult

fun SearchResultRoom.toSearchResult(): SearchResult = SearchResult(
    title = this.title,
    link = this.link,
    description = this.description
)

fun GoogleSearchResponse.toRoom(query: String): List<SearchResultRoom> {
    return this.results.map {
        SearchResultRoom(
            title = it.title,
            link = it.link,
            description = it.description,
            query = query
        )
    }
}