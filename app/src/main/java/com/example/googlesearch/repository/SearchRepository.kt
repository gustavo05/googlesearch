package com.example.googlesearch.repository

import com.example.googlesearch.data.database.dao.SearchResultDao
import com.example.googlesearch.data.database.model.SearchResultRoom
import com.example.googlesearch.data.network.GoogleApi
import com.example.googlesearch.data.network.model.GoogleSearchResponse
import com.example.googlesearch.model.SearchResult
import com.example.googlesearch.repository.mappers.toRoom
import com.example.googlesearch.repository.mappers.toSearchResult
import com.example.googlesearch.repository.model.Result
import kotlinx.coroutines.flow.*

class SearchRepository(
    private val googleApi: GoogleApi,
    private val searchResultDao: SearchResultDao
) {

    fun search(query: String): Flow<Result<List<SearchResult>>> =
        fetchDataTemplate(
            fetchFromLocal = { searchResultDao.getResults(query, limit = 100) },
            shouldFetchFromRemote = { true },
            fetchFromRemote = { googleApi.search(query, 100) },
            saveRemoteData = { searchResultDao.insert(it.toRoom(query)) },
            deleteData = { searchResultDao.delete() },
            mapper = { it.map { item -> item.toSearchResult() } }
        )
            .map { if (it is Result.Success && it.result.isEmpty()) Result.NotFound else it }
            .catch { emit(Result.Error) }

}