package com.example.googlesearch.repository.mappers

import com.example.googlesearch.data.database.model.ImageResultRoom
import com.example.googlesearch.data.network.model.GoogleImageResponse
import com.example.googlesearch.model.ImageResult


fun ImageResultRoom.toImageResult(): ImageResult =
    ImageResult(
        src = this.src,
        alt = this.alt,
        href = this.href,
        title = this.title
    )

fun GoogleImageResponse.toRoom(query:String): List<ImageResultRoom> =
    this.images.map {
        ImageResultRoom(
            src = it.image.src,
            alt = it.image.alt,
            href = it.link.href,
            title = it.link.title,
            query = query
        )
    }