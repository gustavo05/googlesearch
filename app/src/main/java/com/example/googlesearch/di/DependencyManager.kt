package com.example.googlesearch.di

import android.content.Context
import com.example.googlesearch.data.database.GoogleSearchDatabase
import com.example.googlesearch.data.database.dao.ImageResultDao
import com.example.googlesearch.data.database.dao.SearchResultDao
import com.example.googlesearch.data.network.GoogleApi
import com.example.googlesearch.data.network.NetworkInterceptor
import com.example.googlesearch.repository.ImageRepository
import com.example.googlesearch.repository.SearchRepository
import com.example.googlesearch.usecase.SearchUseCase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

fun provideGoogleApi(): GoogleApi =
    Retrofit.Builder()
        .baseUrl("https://google-search3.p.rapidapi.com/api/v1/")
        .client(
            OkHttpClient.Builder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .addInterceptor(NetworkInterceptor())
                .build()
        )
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(GoogleApi::class.java)

fun provideSearchDao(context: Context): SearchResultDao =
    GoogleSearchDatabase.getInstance(context).searchDao

fun provideImageDao(context: Context): ImageResultDao =
    GoogleSearchDatabase.getInstance(context).imageDao

fun provideSearchRepository(context: Context): SearchRepository =
    SearchRepository(
        provideGoogleApi(),
        provideSearchDao(context)
    )

fun provideImageRepository(context: Context): ImageRepository =
    ImageRepository(
        provideGoogleApi(),
        provideImageDao(context)
    )

fun provideSearchUseCase(context: Context): SearchUseCase =
    SearchUseCase(
        provideSearchRepository(context),
        provideImageRepository(context)
    )
