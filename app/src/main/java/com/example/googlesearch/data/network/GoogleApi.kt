package com.example.googlesearch.data.network

import com.example.googlesearch.data.network.model.GoogleImageResponse
import com.example.googlesearch.data.network.model.GoogleSearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GoogleApi {

    @GET("search/q={q}&num={num}")
    suspend fun search(
        @Path("q") query: String,
        @Path("num") num: Int
    ): Response<GoogleSearchResponse>

    @GET("images/q={q}")
    suspend fun images(@Path("q") query: String): Response<GoogleImageResponse>
}