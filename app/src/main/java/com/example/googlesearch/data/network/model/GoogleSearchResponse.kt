package com.example.googlesearch.data.network.model

import com.squareup.moshi.Json

data class GoogleSearchResponse(
    @field:Json(name = "results") val results: List<GoogleSearchResult>
)

data class GoogleSearchResult(
    @field:Json(name = "title") val title: String,
    @field:Json(name = "link") val link: String,
    @field:Json(name = "description") val description: String,
    @field:Json(name = "g_review_stars") val reviewStars: String
)