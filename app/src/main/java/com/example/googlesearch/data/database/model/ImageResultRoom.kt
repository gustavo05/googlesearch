package com.example.googlesearch.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["query", "title"])
data class ImageResultRoom(
    val src: String,
    val query: String,
    val alt: String,
    val href: String,
    val title: String
)