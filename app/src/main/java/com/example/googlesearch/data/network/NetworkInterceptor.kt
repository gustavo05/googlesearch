package com.example.googlesearch.data.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class NetworkInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original: Request = chain.request()

        val request: Request = original.newBuilder()
            .header("x-rapidapi-key", "3563136a97mshc9f6e9c377baccep125973jsn9b9d738e841b")
            .header("x-rapidapi-host", "google-search3.p.rapidapi.com")
            .method(original.method, original.body)
            .build()

        return chain.proceed(request)
    }
}