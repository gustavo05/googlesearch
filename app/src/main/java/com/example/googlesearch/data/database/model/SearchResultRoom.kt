package com.example.googlesearch.data.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(primaryKeys = ["query", "title"])
data class SearchResultRoom(
    val title: String,
    val query: String,
    val link: String,
    val description: String
)