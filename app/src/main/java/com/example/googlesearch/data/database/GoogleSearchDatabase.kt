package com.example.googlesearch.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.googlesearch.data.database.dao.ImageResultDao
import com.example.googlesearch.data.database.dao.SearchResultDao
import com.example.googlesearch.data.database.model.ImageResultRoom
import com.example.googlesearch.data.database.model.SearchResultRoom

@Database(
    entities = [SearchResultRoom::class, ImageResultRoom::class],
    version = 1,
    exportSchema = false
)
abstract class GoogleSearchDatabase : RoomDatabase() {

    abstract val searchDao: SearchResultDao
    abstract val imageDao: ImageResultDao

    companion object {

        @Volatile
        private var INSTANCE: GoogleSearchDatabase? = null

        fun getInstance(context: Context): GoogleSearchDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        GoogleSearchDatabase::class.java,
                        "google_search_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}