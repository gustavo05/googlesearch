package com.example.googlesearch.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.googlesearch.data.database.model.ImageResultRoom
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageResultDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(results: List<ImageResultRoom>)

    @Query("DELETE FROM ImageResultRoom")
    suspend fun delete()

    @Query("SELECT * FROM ImageResultRoom WHERE query = :query")
    fun getResults(query: String): Flow<List<ImageResultRoom>>
}