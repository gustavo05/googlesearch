package com.example.googlesearch.data.network.model

import com.squareup.moshi.Json

data class GoogleImageResponse(
    @field:Json(name = "image_results") val images: List<GoogleImageResult>
)

data class GoogleImageResult(
    @field:Json(name = "image") val image: GoogleImage,
    @field:Json(name = "link") val link: GoogleImageReference
)

data class GoogleImage(
    @field:Json(name = "src") val src: String,
    @field:Json(name = "alt") val alt: String
)

data class GoogleImageReference(
    @field:Json(name = "href") val href: String,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "domain") val domain: String
)