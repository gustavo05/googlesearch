package com.example.googlesearch.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.googlesearch.data.database.model.SearchResultRoom
import kotlinx.coroutines.flow.Flow

@Dao
interface SearchResultDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(results: List<SearchResultRoom>)

    @Query("DELETE FROM SearchResultRoom")
    suspend fun delete()


    @Query("SELECT * FROM SearchResultRoom WHERE query = :query LIMIT :limit")
    fun getResults(query: String, limit: Int): Flow<List<SearchResultRoom>>

}