package com.example.googlesearch.usecase

import android.util.Log
import com.example.googlesearch.model.SearchUIModel
import com.example.googlesearch.model.UIState
import com.example.googlesearch.repository.ImageRepository
import com.example.googlesearch.repository.SearchRepository
import com.example.googlesearch.repository.model.Result
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.map

class SearchUseCase(
    private val searchRepository: SearchRepository,
    private val imageRepository: ImageRepository
) {


    fun execute(query: String): Flow<SearchUIModel> {
        Log.d("EXECUTE", "QUERY -> $query")
        return searchRepository.search(query)
            .combine(imageRepository.search(query)) { search, images -> Pair(search, images) }
            .map {
                SearchUIModel(
                    searchResult = it.first.toUIState(),
                    imageResult = it.second.toUIState()
                )
            }
    }


    private fun <T> Result<T>.toUIState(): UIState<T> =
        when (this) {
            is Result.Error -> UIState.Error
            is Result.NotFound -> UIState.NotFound
            is Result.Loading -> UIState.Loading(this.result)
            is Result.Success -> UIState.Success(this.result)
        }
}