package com.example.googlesearch

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.googlesearch.ui.examples.ReactiveFragment
import com.example.googlesearch.ui.main.SearchFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ReactiveFragment.newInstance())
                .commitNow()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.searchMenuItem -> {
                goToSearch()
                true
            }
            R.id.reactiveMenuItem -> {
                goToReactive()
                true
            }
            else ->super.onOptionsItemSelected(item)
        }
    }

    private fun goToSearch() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SearchFragment.newInstance())
            .commitNow()
    }

    private fun goToReactive() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ReactiveFragment.newInstance())
            .commitNow()
    }
}