package com.example.googlesearch.model

sealed class UIState<out T> {
    data class Success<out T>(val result: T) : UIState<T>()
    data class Loading<out T>(val result: T?) : UIState<T>()
    object Error : UIState<Nothing>()
    object NotFound : UIState<Nothing>()
}