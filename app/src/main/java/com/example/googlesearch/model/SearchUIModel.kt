package com.example.googlesearch.model

data class SearchUIModel(
    val searchResult: UIState<List<SearchResult>>,
    val imageResult: UIState<List<ImageResult>>
)