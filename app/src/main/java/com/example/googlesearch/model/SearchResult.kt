package com.example.googlesearch.model

data class SearchResult(
    val title: String,
    val link: String,
    val description: String
)