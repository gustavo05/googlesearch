package com.example.googlesearch.model

data class ImageResult(
    val src: String,
    val alt: String,
    val href: String,
    val title: String
)